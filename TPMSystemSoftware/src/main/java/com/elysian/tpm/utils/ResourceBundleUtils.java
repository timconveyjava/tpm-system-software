package com.elysian.tpm.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.context.FacesContext;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class ResourceBundleUtils {
	private ResourceBundleUtils() {
	}

	private static String bundleName = "msgs";
	private static String name = "messages";
	private static ResourceBundle bundle;

	private static final Logger LOGGER = LogManager.getLogger(ResourceBundleUtils.class.getName());

	static {
		final FacesContext fctx = FacesContext.getCurrentInstance();
		bundle = fctx.getApplication().getResourceBundle(fctx, bundleName);
	}

	public static String getMessage(String key) {
		final FacesContext fctx = FacesContext.getCurrentInstance();
		final ResourceBundle resBundle = fctx.getApplication().getResourceBundle(fctx, bundleName);
		String result = key;
		try {
			result = resBundle.getString(key);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage());

		}
		return result;

	}

	public static String getMessageLocaleSelected(String key, Locale locale) {
		final ResourceBundle resourceLocale = ResourceBundle.getBundle(name, locale);
		String result = key;
		try {
			result = resourceLocale.getString(key);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage());

		}
		return result;
	}

	public static String getString(String key, Object... params) {
		if ((key == null) || key.isEmpty()) {
			return "";
		}
		try {
			return MessageFormat.format(bundle.getString(key), params);
		} catch (final MissingResourceException e) {
			return "?? " + key + " ?? Key not found.";
		}
	}
}
