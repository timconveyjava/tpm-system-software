package com.elysian.tpm.utils;

import org.primefaces.component.growl.Growl;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;

public class FacesUtils implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3285060482604720509L;

	public static void addMessage(String message, Growl theMessagesGrowl) {
		theMessagesGrowl.setLife(5000);
		FacesContext root = FacesContext.getCurrentInstance();
		FacesMessage fm = new FacesMessage(message);
		fm.setDetail("");
		root.addMessage(null, fm);
	}

	public static void addErrorMessage(String message, String stackTrace, Growl theMessagesGrowl) {
		theMessagesGrowl.setLife(10800000); // 3 hours
		FacesContext root = FacesContext.getCurrentInstance();
		FacesMessage error;
		if (stackTrace != null) {
			error = new FacesMessage(message, stackTrace);
			error.setSeverity(FacesMessage.SEVERITY_ERROR);
			root.addMessage(null, error);
		} else {
			error = new FacesMessage(message);
			error.setSeverity(FacesMessage.SEVERITY_ERROR);
			root.addMessage(null, error);
		}
	}

	public static void addMessageWithSummary(String sum, String detail) {
		FacesContext root = FacesContext.getCurrentInstance();
		FacesMessage faceMessage = new FacesMessage(FacesMessage.SEVERITY_INFO, sum, detail);
		root.addMessage(null, faceMessage);
	}

	public static void addErrorMessageWithSummary(String details, String stackTrace, Growl theMessagesGrowl) {
		theMessagesGrowl.setLife(30000);
		FacesContext root = FacesContext.getCurrentInstance();
		FacesMessage fm;
		if (stackTrace != null)
			fm = new FacesMessage(ResourceBundleUtils.getMessage("severity_error"), stackTrace);
		else
			fm = new FacesMessage(ResourceBundleUtils.getMessage("severity_error"));
		fm.setSeverity(FacesMessage.SEVERITY_ERROR);
		fm.setDetail(details);

		root.addMessage(null, fm);
	}

	public static void addWarnMessageWithSummary(String details, Growl theMessagesGrowl) {
		theMessagesGrowl.setLife(10000);
		FacesContext root = FacesContext.getCurrentInstance();

		FacesMessage fm = new FacesMessage(ResourceBundleUtils.getMessage("severity_warn"));
		fm.setSeverity(FacesMessage.SEVERITY_WARN);
		fm.setDetail(details);

		root.addMessage(null, fm);
	}

	public static void addInfoMessageWithSummary(String details, Growl theMessagesGrowl) {
		theMessagesGrowl.setLife(10000);
		FacesContext root = FacesContext.getCurrentInstance();

		FacesMessage fm = new FacesMessage(ResourceBundleUtils.getMessage("severity_info"));
		fm.setSeverity(FacesMessage.SEVERITY_INFO);
		fm.setDetail(details);

		root.addMessage(null, fm);
	}
}
