package com.elysian.tpm.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

@SuppressWarnings({ "unused", "serial" })
public class LogPhaseListener2 implements PhaseListener {

	private long startTimeP1;
	private long startTimeP2;
	private long startTimeP3;
	private long startTimeP4;
	private long startTimeP5;
	private long startTimeP6;
	private long timeP1;
	private long timeP2;
	private long timeP3;
	private long timeP4;
	private long timeP5;
	private long timeP6;
	private long timeAll;

	private static final Logger LOGGER = LogManager.getLogger(LogPhaseListener2.class);

	@Override
	public void afterPhase(PhaseEvent event) {

		if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
			timeP1 = (long) ((System.nanoTime() - startTimeP1) * 0.000001);
			LOGGER.debug("|     Phase 1 Execution time -- RESTORE_VIEW -- " + timeP1 + "ms");
		}
		if (event.getPhaseId() == PhaseId.APPLY_REQUEST_VALUES) {
			timeP2 = (long) ((System.nanoTime() - startTimeP2) * 0.000001);
			LOGGER.debug("|     Phase 2 Execution time -- APPLY_REQUEST_VALUES -- " + timeP2 + "ms");
		}
		if (event.getPhaseId() == PhaseId.PROCESS_VALIDATIONS) {
			timeP3 = (long) ((System.nanoTime() - startTimeP3) * 0.000001);
			LOGGER.debug("|     Phase 3 Execution time -- PROCESS_VALIDATIONS -- " + timeP3 + "ms");
		}
		if (event.getPhaseId() == PhaseId.UPDATE_MODEL_VALUES) {
			timeP4 = (long) ((System.nanoTime() - startTimeP4) * 0.000001);
			LOGGER.debug("|     Phase 4 Execution time -- UPDATE_MODEL_VALUES -- " + timeP4 + "ms");
		}
		if (event.getPhaseId() == PhaseId.INVOKE_APPLICATION) {
			timeP5 = (long) ((System.nanoTime() - startTimeP5) * 0.000001);
			LOGGER.debug("|     Phase 5 Execution time -- INVOKE_APPLICATION -- " + timeP5 + "ms");
		}
		if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
			timeP6 = (long) ((System.nanoTime() - startTimeP6) * 0.000001);
			timeAll = (long) ((System.nanoTime() - startTimeP1) * 0.000001);

			LOGGER.debug("|     Phase 6 Execution time -- RENDER_RESPONSE -- " + timeP6 + "ms");
			LOGGER.debug("|     Phase X Execution time -- ALL_PHASES -- " + timeAll + "ms");
		}
	}

	@Override
	public void beforePhase(PhaseEvent event) {

		if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
			startTimeP1 = System.nanoTime();
		}
		if (event.getPhaseId() == PhaseId.APPLY_REQUEST_VALUES) {
			startTimeP2 = System.nanoTime();
		}
		if (event.getPhaseId() == PhaseId.PROCESS_VALIDATIONS) {
			startTimeP3 = System.nanoTime();
		}
		if (event.getPhaseId() == PhaseId.UPDATE_MODEL_VALUES) {
			startTimeP4 = System.nanoTime();
		}
		if (event.getPhaseId() == PhaseId.INVOKE_APPLICATION) {
			startTimeP5 = System.nanoTime();
		}
		if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
			startTimeP6 = System.nanoTime();
		}
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}
}
