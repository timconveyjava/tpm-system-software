package com.elysian.tpm.utils;


import com.elysian.tpm.beans.ComboboxList;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.*;


public class LanguageBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String localeCode;
    private Locale locale;
    protected Map<String, String> changeSessionAttr = new LinkedHashMap<String, String>();
    protected Map<String, Object> countries = new LinkedHashMap<String, Object>();

    private static final Locale localeEn = Locale.ENGLISH;
    private static final Locale localeRo = new Locale("ro", "RO");
    private static final Locale localeHu = new Locale("hu", "HU");

    private static final String localeCodeEn = "en";
    private static final String localeCodeRo = "ro_RO";
    private static final String localeCodeHu = "hu_HU";

    public LanguageBean() {
        locale = localeHu;
        localeCode = localeCodeHu;
        countries = new LinkedHashMap<String, Object>();
        changeSessionAttr = new LinkedHashMap<String, String>();
    }

    public LanguageBean(String languageId, List<ComboboxList> languageCombos) {
        countries = new LinkedHashMap<String, Object>();
        changeSessionAttr = new LinkedHashMap<String, String>();

        switch (Integer.parseInt(languageId)) {
            case 1: {
                locale = localeEn;
                localeCode = localeCodeEn;
                break;
            }
            case 2: {
                locale = localeHu;
                localeCode = localeCodeHu;
                break;
            }
            default: {
                locale = localeRo;
                localeCode = localeCodeRo;
                break;
            }
        }

        countries = new LinkedHashMap<String, Object>();
        changeSessionAttr = new LinkedHashMap<String, String>();
        for (int i = 0; i < languageCombos.size(); i++) {

            switch (Integer.parseInt(languageCombos.get(i).getValue())) {
                case 1: {
                    countries.put("English", localeEn);
                    changeSessionAttr.put(localeCodeEn, languageCombos.get(i).getValue());
                    break;
                }
                case 2: {
                    countries.put("Magyar", localeEn);
                    changeSessionAttr.put(localeCodeEn, languageCombos.get(i).getValue());
                    break;
                }
                default: {
                    countries.put("Romana", localeRo);
                    changeSessionAttr.put(localeCodeRo, languageCombos.get(i).getValue());
                    break;
                }
            }
        }

    }

    public LanguageBean(List<ComboboxList> languageCombos) {
        locale = localeHu;
        localeCode = localeCodeHu;
        countries = new LinkedHashMap<>();
        changeSessionAttr = new LinkedHashMap<>();
        for (int i = 0; i < languageCombos.size(); i++) {

            switch (Integer.parseInt(languageCombos.get(i).getValue())) {
                case 1: {
                    countries.put("English", localeEn);
                    changeSessionAttr.put(localeCodeEn, languageCombos.get(i).getValue());
                    break;
                }
                case 2: {
                    countries.put("Magyar", localeHu);
                    changeSessionAttr.put(localeCodeHu, languageCombos.get(i).getValue());
                    break;
                }
                case 3: {
                    countries.put("Romana", localeRo);

                    changeSessionAttr.put(localeCodeRo, languageCombos.get(i).getValue());
                    break;
                }
              /*  default: {
                    countries.put("English", localeEn);
                    changeSessionAttr.put(localeCodeEn, languageCombos.get(i).getValue());
                    break;
                }*/
            }
        }
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }


    public void changeLocaleByFlag(String value) {
        for (final Map.Entry<String, Object> entry : countries.entrySet()) {

            if (entry.getValue().toString().equals(value)) {
                setLocale((Locale) entry.getValue(), value);
                setLocaleCode(value);

            }

        }
    }

    public Map<String, Object> getCountries() {
        return countries;
    }

    public void setCountries(Map<String, Object> countries) {
        this.countries = countries;
    }

    public Locale getLocale() {
        return locale;
    }

    public String setLocale(Locale locale, String foundLocale) {
        boolean isUserLogged = false;

        this.locale = locale;
        final FacesContext fctx = FacesContext.getCurrentInstance();

        final UIViewRoot viewRoot = fctx.getViewRoot();
        viewRoot.setLocale(locale);
        final Set<String> objSet = changeSessionAttr.keySet();
        final Iterator<String> objItr = objSet.iterator();
        while (objItr.hasNext()) {
            final String compareLocale = objItr.next();
            if (compareLocale.equals(foundLocale)) {

                final HttpSession hsession = (HttpSession) fctx.getExternalContext().getSession(false);

                hsession.setAttribute("language", changeSessionAttr.get(compareLocale));

                /*
                for (Enumeration enumm = hsession.getAttributeNames(); enumm.hasMoreElements(); ) {
                    if (enumm.nextElement().equals("logged_user_id")) {
                        isUserLogged = true;
                    }
                }

                if (isUserLogged) {
                    final int user_id = (int) hsession.getAttribute("logged_user_id");
                    final UserService userService = new UserService();
                    userService.updateLanguageForUser(user_id, changeSessionAttr.get(compareLocale));
                }*/

                final String viewId = viewRoot.getViewId();

                updateViews(fctx, viewId);

            }
        }
        return null;
    }

    public void setLocaleFromLogin(String localeUser) {
        for (final Map.Entry<String, Object> entry : countries.entrySet()) {

            if (entry.getValue().toString().equals(localeUser)) {
                setLocale((Locale) entry.getValue(), localeUser);

            }

        }

        final FacesContext fctx = FacesContext.getCurrentInstance();
        final UIViewRoot viewRoot = fctx.getViewRoot();
        viewRoot.setLocale(locale);

        final Set<String> objSet = changeSessionAttr.keySet();
        final Iterator<String> objItr = objSet.iterator();
        while (objItr.hasNext()) {
            final String compareLocale = objItr.next();
            if (compareLocale.equals(localeUser)) {
                final String viewId = viewRoot.getViewId();

                updateViews(fctx, viewId);

            }
        }
    }

    private void updateViews(final FacesContext fctx, final String viewId) {
        switch (viewId.substring(1, viewId.length())) {

       /*     case "CompanyAdminPage.xhtml": {
                fctx.getApplication().evaluateExpressionGet(fctx, "#{adminController}", AdminController.class).reinit();

                break;

            }*/
        }
    }
}
