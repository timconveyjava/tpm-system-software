package com.elysian.tpm.utils;

import org.omnifaces.util.Faces;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;

public class SessionExpiredPhaseListener implements PhaseListener {

    private static final long serialVersionUID = 575220391958653687L;

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    @Override
    public void beforePhase(PhaseEvent phaseEvent) {
        HttpServletRequest httpRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (httpRequest.getRequestedSessionId() != null && !httpRequest.isRequestedSessionIdValid()) {
            // check if the session is expired or not
            String facesRequestHeader = httpRequest.getHeader("Faces-Request");
            boolean isAjaxRequest = facesRequestHeader != null && facesRequestHeader.equals("partial/ajax");

            if (isAjaxRequest) {
                // caught an Ajax request in an expired session
                // then redirect to the login page
                Faces.redirect(Faces.getRequestContextPath() + "/SessionExpired.xhtml", "");
            }
        }
    }

    @Override
    public void afterPhase(PhaseEvent phaseEvent) {

    }
}
