package com.elysian.tpm.utils;

import com.elysian.tpm.services.UserService;
import org.jboss.weld.context.ejb.Ejb;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;

@Named
@RequestScoped
public class UserNameAvailableValidator implements Validator<Object> {

    @Ejb
    private UserService userService;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        userService = new UserService();
        String userName = (String) value;

  /*      if (userName.contains(" ")) {
            FacesMessage error = new FacesMessage(ResourceBundleUtils.getMessage("UserIdHasSpaces"));
            error.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(error);
        }*/

        if (userService.checkUserAtLogin(userName)) {
            FacesMessage error = new FacesMessage(com.elysian.tpm.utils.ResourceBundleUtils.getMessage("UserIdNotAvailable"));
            error.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(error);
        }
    }

}
