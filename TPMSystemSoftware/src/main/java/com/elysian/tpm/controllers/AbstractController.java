package com.elysian.tpm.controllers;

import com.elysian.tpm.beans.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AbstractController extends com.elysian.tpm.controllers.MessageController implements Serializable {

    protected User loggedUser;
    private Logger LOGGER = LogManager.getLogger(AbstractController.class);

    public AbstractController() {
    }


    public String formatDate(final Object myDate) {
        if (myDate == null) {
            return "";
        } else {
            if (myDate instanceof Date) {
                return new SimpleDateFormat("dd/MM/yyyy HH:mm").format((Date) myDate);
            } else {
                return (String) myDate;
            }
        }
    }
    
    
}
