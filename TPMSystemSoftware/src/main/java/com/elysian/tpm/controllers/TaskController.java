package com.elysian.tpm.controllers;

import com.elysian.tpm.services.TaskService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ViewScoped
@Named
public class TaskController extends AbstractController implements Serializable {

    private Logger LOGGER = LogManager.getLogger(TaskController.class);

    private List<String> datasource_frequency = new ArrayList<>();
    private List<String> datasource_line = new ArrayList<>();
    private List<String> datasource_machine_name = new ArrayList<>();
    private List<String> datasource_machine_number = new ArrayList<>();
    //private List<Integer> datasource_time = new ArrayList<>();
    private List<String> datasource_task_type = new ArrayList<>();
    private List<String> datasource_tool_name = new ArrayList<>();
    private List<String> datasource_tool_number = new ArrayList<>();

    private TaskService taskService = new TaskService();


    private String frequency_selected = null;
    private String line_selected = null;
    private String machine_name_selected = null;
    private String machine_number_selected = null;
    //private String time_selected = null;
    private String task_type_selected = null;
    private String tool_name_selected = null;
    private String tool_number_selected = null;

    public TaskController() {
        final HttpSession hsession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
                .getSession(false);

        datasource_frequency = taskService.getFrequencyList();
        datasource_line = taskService.getLineList();
        datasource_machine_name = taskService.getMachineNameList();
        datasource_machine_number = taskService.getMachineNumberList();
        //datasource_time = taskService.getTaskDuration();
        datasource_task_type = taskService.getTaskTypeList();
        datasource_tool_name = taskService.getToolNameList();
        datasource_tool_number = taskService.getToolNumberList();

    }

    public void addTask() {
//        SQLResult result;
//        newUser.setUpdated_by(loggedUser.getUser_id());
//        result = userService.addUser(newUser);
//
//        if (result.isOk()) {
//            addInfoMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()));
//            datasource = userService.getUsersList();
//            newUser = new User();
//
//        } else {
//            addErrorMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()),
//                    result.getStackTrace());
//        }
    }

    public List<String> getDatasource_frequency() {
        return datasource_frequency;
    }

    public void setDatasource_frequency(List<String> datasource_frequency) {
        this.datasource_frequency = datasource_frequency;
    }

    public List<String> getDatasource_line() {
        return datasource_line;
    }

    public void setDatasource_line(List<String> datasource_line) {
        this.datasource_line = datasource_line;
    }

    public List<String> getDatasource_machine_name() {
        return datasource_machine_name;
    }

    public void setDatasource_machine_name(List<String> datasource_machine_name) {
        this.datasource_machine_name = datasource_machine_name;
    }

    public List<String> getDatasource_machine_number() {
        return datasource_machine_number;
    }

    public void setDatasource_machine_number(List<String> datasource_machine_number) {
        this.datasource_machine_number = datasource_machine_number;
    }

//    public List<Integer> getDatasource_time() {
//        return datasource_time;
//    }
//
//    public void setDatasource_time(List<Integer> datasource_time) {
//        this.datasource_time = datasource_time;
//    }

    public List<String> getDatasource_task_type() {
        return datasource_task_type;
    }

    public void setDatasource_task_type(List<String> datasource_task_type) {
        this.datasource_task_type = datasource_task_type;
    }

    public List<String> getDatasource_tool_name() {
        return datasource_tool_name;
    }

    public void setDatasource_tool_name(List<String> datasource_tool_name) {
        this.datasource_tool_name = datasource_tool_name;
    }

    public List<String> getDatasource_tool_number() {
        return datasource_tool_number;
    }

    public void setDatasource_tool_number(List<String> datasource_tool_number) {
        this.datasource_tool_number = datasource_tool_number;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public String getFrequency_selected() {
        return frequency_selected;
    }

    public void setFrequency_selected(String frequency_selected) {
        this.frequency_selected = frequency_selected;
    }

    public String getLine_selected() {
        return line_selected;
    }

    public void setLine_selected(String line_selected) {
        this.line_selected = line_selected;
    }

    public String getMachine_name_selected() {
        return machine_name_selected;
    }

    public void setMachine_name_selected(String machine_name_selected) {
        this.machine_name_selected = machine_name_selected;
    }

    public String getMachine_number_selected() {
        return machine_number_selected;
    }

    public void setMachine_number_selected(String machine_number_selected) {
        this.machine_number_selected = machine_number_selected;
    }
//
//    public String getTime_selected() {
//        return time_selected;
//    }
//
//    public void setTime_selected(String time_selected) {
//        this.time_selected = time_selected;
//    }

    public String getTask_type_selected() {
        return task_type_selected;
    }

    public void setTask_type_selected(String task_type_selected) {
        this.task_type_selected = task_type_selected;
    }

    public String getTool_name_selected() {
        return tool_name_selected;
    }

    public void setTool_name_selected(String tool_name_selected) {
        this.tool_name_selected = tool_name_selected;
    }

    public String getTool_number_selected() {
        return tool_number_selected;
    }

    public void setTool_number_selected(String tool_number_selected) {
        this.tool_number_selected = tool_number_selected;
    }
}
