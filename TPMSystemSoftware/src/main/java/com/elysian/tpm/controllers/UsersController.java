package com.elysian.tpm.controllers;

import com.elysian.tpm.beans.User;
import com.elysian.tpm.mybatis.SQLResult;
import com.elysian.tpm.services.UserService;
import com.elysian.tpm.utils.ResourceBundleUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ViewScoped
@Named
public class UsersController extends AbstractController implements Serializable {

    private Logger LOGGER = LogManager.getLogger(UsersController.class);
    private List<User> datasource = new ArrayList<>();
    private List<User> selectionList;
    private UserService userService = new UserService();


    private User newUser = new User();
    private User editUser;
    private User editUserRole;

    public UsersController() {
        final HttpSession hsession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
                .getSession(false);
        loggedUser = (User) hsession.getAttribute("logged_user");
        LOGGER.debug("WipartController - constructor - logged_user: " + loggedUser.getUser_id());
        datasource = userService.getUsersList();
    }

    public void initializeAddUser() {
        newUser = new User();
    }

    public void initializeEditUser() {
        if (selectionList != null && selectionList.size() > 0) {
            User user = selectionList.get(0);
            editUser = new User(user.getUser_id(), user.getUsername(), user.getFirst_name(), user.getLast_name(), user.getEmail());
        }
    }

    public void initializeEditUserRole() {
        if (selectionList != null && selectionList.size() > 0) {
            User user = selectionList.get(0);
            editUserRole = new User(user.getUser_id(), user.getUsername(), user.getT_role());
        }
    }

    public void enableUser() {
        SQLResult result;
        result = userService.updateStatus(selectionList.get(0).getUser_id(), 1, loggedUser.getUser_id());

        if (result.isOk()) {
            addInfoMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()));
            datasource = userService.getUsersList();
            selectionList.clear();
        } else {
            addErrorMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()),
                    result.getStackTrace());
        }
    }

    public void disableUser() {
        SQLResult result;
        result = userService.updateStatus(selectionList.get(0).getUser_id(), 2, loggedUser.getUser_id());

        if (result.isOk()) {
            addInfoMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()));
            datasource = userService.getUsersList();
            selectionList.clear();
        } else {
            addErrorMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()),
                    result.getStackTrace());
        }
    }

    public void addUser() {
        SQLResult result;
        newUser.setUpdated_by(loggedUser.getUser_id());
        result = userService.addUser(newUser);

        if (result.isOk()) {
            addInfoMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()));
            datasource = userService.getUsersList();
            newUser = new User();

        } else {
            addErrorMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()),
                    result.getStackTrace());
        }
    }

    public void updateUser() {
        boolean everythingOK = true;
        final FacesContext faces = FacesContext.getCurrentInstance();

        if (!editUser.getUsername().toLowerCase().equals(selectionList.get(0).getUsername().toLowerCase())) {
            boolean userExists = userService.checkUserAtLogin(editUser.getUsername());
            if (userExists) {
                everythingOK = false;
                faces.validationFailed();
                final FacesMessage selectionMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ResourceBundleUtils.getMessage("UserIdNotAvailable"), null);
                faces.addMessage(null, selectionMessage);
            }
        }
        if (everythingOK) {
            SQLResult result;
            editUser.setUpdated_by(loggedUser.getUser_id());
            result = userService.editUser(editUser);

            if (result.isOk()) {
                addInfoMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()));
                datasource = userService.getUsersList();

            } else {
                addErrorMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()),
                        result.getStackTrace());
            }
        }
    }

    public void updateUserRole() {
        boolean everythingOK = true;
        final FacesContext faces = FacesContext.getCurrentInstance();

        if (!editUserRole.getUsername().toLowerCase().equals(selectionList.get(0).getUsername().toLowerCase())) {
            boolean userExists = userService.checkUserAtLogin(editUserRole.getUsername());
            if (userExists) {
                everythingOK = false;
                faces.validationFailed();
                final FacesMessage selectionMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        ResourceBundleUtils.getMessage("UserIdNotAvailable"), null);
                faces.addMessage(null, selectionMessage);
            }
        }
        if (everythingOK) {
            SQLResult result;
            editUserRole.setUpdated_by(loggedUser.getUser_id());
            result = userService.editUserRole(editUserRole);

            if (result.isOk()) {
                addInfoMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()));
                datasource = userService.getUsersList();

            } else {
                addErrorMessageWithSummary(ResourceBundleUtils.getMessage(result.getResultMessage()),
                        result.getStackTrace());
            }
        }
    }

    public boolean disabledBtn() {
        boolean result = false;

        if ((selectionList == null) || selectionList.size() != 1) {
            result = true;
            return result;
        }

        if (loggedUser.getUser_id() == selectionList.get(0).getUser_id()) {
            result = true;
        }

        return result;
    }

    public boolean disabledActivateUser() {
        boolean result = false;

        if ((selectionList == null) || selectionList.size() != 1) {
            result = true;
            return result;
        }
        if (selectionList.get(0).getT_status() == 1) {
            result = true;
        }
        return result;
    }

    public boolean disabledDeactivateUser() {
        boolean result = false;

        if ((selectionList == null) || selectionList.size() != 1) {
            result = true;
            return result;
        }
        if (selectionList.get(0).getT_status() == 2) {
            result = true;
        }
        return result;
    }

    public void selectionListener() {
    }

    public List<User> getDatasource() {
        return datasource;
    }

    public void setDatasource(List<User> datasource) {
        this.datasource = datasource;
    }

    public List<User> getSelectionList() {
        return selectionList;
    }

    public void setSelectionList(List<User> selectionList) {
        this.selectionList = selectionList;
    }

    public User getNewUser() {
        return newUser;
    }

    public void setNewUser(User newUser) {
        this.newUser = newUser;
    }

    public User getEditUser() {
        return editUser;
    }

    public User getEditUserRole() {
        return editUserRole;
    }

    public void setEditUser(User editUser) {
        this.editUser = editUser;
    }
}
