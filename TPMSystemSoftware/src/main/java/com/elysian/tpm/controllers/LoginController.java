package com.elysian.tpm.controllers;


import com.elysian.tpm.beans.ComboboxList;
import com.elysian.tpm.beans.User;
import com.elysian.tpm.services.UserService;
import com.elysian.tpm.utils.LanguageBean;
import com.elysian.tpm.utils.ResourceBundleUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SessionScoped
@Named
public class LoginController extends AbstractController implements Serializable {

    static Logger LOGGER = LogManager.getLogger(LoginController.class.getName());

    private boolean loggedIn = false;
    private String windowsAuthUser;
    private LanguageBean langBean;

    private boolean userAuth = false;
    private List<ComboboxList> languageCombos;

    private String error_message;
    private User loggedInUser;

    public LoginController() {
        super();
        languageCombos = new ArrayList<>();
        languageCombos.add(new ComboboxList("1", "English"));
        languageCombos.add(new ComboboxList("2", "Magyar"));
        languageCombos.add(new ComboboxList("3", "Romana"));
        langBean = new LanguageBean(languageCombos);
    }

    @PostConstruct
    public void authe() {
        // get logged in users name from http request
        final HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        if (request.getUserPrincipal() != null) {
            windowsAuthUser = request.getUserPrincipal().getName();
            // extract the login name from the string received in the http servlet request
            windowsAuthUser = windowsAuthUser.substring(windowsAuthUser.indexOf("\\") + 1, windowsAuthUser.length());
            userAuth = true;
        }
        LOGGER.info(windowsAuthUser);

    }

    public void login() {

        UserService userService = new UserService();
        final FacesContext context = FacesContext.getCurrentInstance();

        boolean loginOk = false;
        if (!(context.getMessageList().size() > 0)) {

            loginOk = userService.checkUserAtLogin(windowsAuthUser);

            if (loginOk) {
                final HttpSession hsession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
                        .getSession(false);

                User user = userService.getUserByUsername(windowsAuthUser);

                if (user.getT_status() == 1) {
                    loggedIn = true;
                    hsession.setAttribute("logged_user", user);
                    loggedInUser = user;
                    try {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                    } catch (final IOException e) {
                        LOGGER.error(e.getMessage());
                    }
                } else {
                    error_message = ResourceBundleUtils.getMessage("error_message_inactive");
                    try {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("failed.xhtml");
                    } catch (final IOException e) {
                        LOGGER.error(e.getMessage());
                    }
                }
            } else {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("failed.xhtml");
                } catch (final IOException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }
    }

    public void logout() {
        loggedIn = false;
        final HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
                .getSession(false);
        if (session != null) {
            session.invalidate();
        }

        //  Faces.redirect("http://localhost:8080/login.xhtml");
        FacesContext.getCurrentInstance().getApplication().getNavigationHandler()
                .handleNavigation(FacesContext.getCurrentInstance(), null, "/login.xhtml?faces-redirect=true");

    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getWindowsAuthUser() {
        return windowsAuthUser;
    }

    public void setWindowsAuthUser(String windowsAuthUser) {
        this.windowsAuthUser = windowsAuthUser;
    }

    public LanguageBean getLangBean() {
        return langBean;
    }

    public void setLangBean(LanguageBean langBean) {
        this.langBean = langBean;
    }

    public boolean isUserAuth() {
        return userAuth;
    }

    public void setUserAuth(boolean userAuth) {
        this.userAuth = userAuth;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
}
