package com.elysian.tpm.controllers;

import com.elysian.tpm.utils.FacesUtils;
import org.primefaces.component.growl.Growl;

import java.io.Serializable;

public class MessageController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    protected transient Growl messagesGrowl = new Growl();

    protected void addInfoMessage(final String theInfoMessage) {
        FacesUtils.addMessage(theInfoMessage, messagesGrowl);
    }

    protected void addErrorMessage(final String theErrorMessage, final String theStackTrace) {
        FacesUtils.addErrorMessage(theErrorMessage, theStackTrace, messagesGrowl);
    }

    protected void addInfoMessageWithSummary(final String theInfoMessage) {
        FacesUtils.addInfoMessageWithSummary(theInfoMessage, messagesGrowl);
    }

    protected void addErrorMessageWithSummary(final String theErrorMessage, final String theStackTrace) {
        FacesUtils.addErrorMessageWithSummary(theErrorMessage, theStackTrace, messagesGrowl);
    }

    protected void addWarnMessageWithSummary(final String theWarnMessage) {
        FacesUtils.addWarnMessageWithSummary(theWarnMessage, messagesGrowl);
    }

    public Growl getMessagesGrowl() {
        return messagesGrowl;
    }

    public void setMessagesGrowl(final Growl messagesGrowl) {
        this.messagesGrowl = messagesGrowl;
    }

}
