package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class Tasks implements Serializable {

    private int task_id;

    private String generated_task_id;

    private Date creation_date;
    private String creator_username;
    private Date update_date;
    private String update_username;

    private int task_type_id;
    private String short_name;

    private int t_task_status_id;
    private String archive_reason;

    private int version_task_id;

    private int factory_id;
    private int area_id;
    private int value_stream_id;
    private int line_id;
    private int machine_id;
    private int tool_id;


    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public String getGenerated_task_id() {
        return generated_task_id;
    }

    public void setGenerated_task_id(String generated_task_id) {
        this.generated_task_id = generated_task_id;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreator_username() {
        return creator_username;
    }

    public void setCreator_username(String creator_username) {
        this.creator_username = creator_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }

    public int getTask_type_id() {
        return task_type_id;
    }

    public void setTask_type_id(int task_type_id) {
        this.task_type_id = task_type_id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public int getT_task_status_id() {
        return t_task_status_id;
    }

    public void setT_task_status_id(int t_task_status_id) {
        this.t_task_status_id = t_task_status_id;
    }

    public String getArchive_reason() {
        return archive_reason;
    }

    public void setArchive_reason(String archive_reason) {
        this.archive_reason = archive_reason;
    }

    public int getVersion_task_id() {
        return version_task_id;
    }

    public void setVersion_task_id(int version_task_id) {
        this.version_task_id = version_task_id;
    }

    public int getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(int factory_id) {
        this.factory_id = factory_id;
    }

    public int getArea_id() {
        return area_id;
    }

    public void setArea_id(int area_id) {
        this.area_id = area_id;
    }

    public int getValue_stream_id() {
        return value_stream_id;
    }

    public void setValue_stream_id(int value_stream_id) {
        this.value_stream_id = value_stream_id;
    }

    public int getLine_id() {
        return line_id;
    }

    public void setLine_id(int line_id) {
        this.line_id = line_id;
    }

    public int getMachine_id() {
        return machine_id;
    }

    public void setMachine_id(int machine_id) {
        this.machine_id = machine_id;
    }

    public int getTool_id() {
        return tool_id;
    }

    public void setTool_id(int tool_id) {
        this.tool_id = tool_id;
    }
}
