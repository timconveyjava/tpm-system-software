package com.elysian.tpm.beans;

import java.io.Serializable;

public class TaskType implements Serializable {

    private int task_type_id;
    private String task_type_desc;

    public TaskType() {
    }

    public TaskType(int task_type_id, String frequency_description) {
        this.task_type_id = task_type_id;
        this.task_type_desc = frequency_description;

    }

    public int getTask_type_id() {
        return task_type_id;
    }

    public void setTask_type_id(int task_type_id) {
        this.task_type_id = task_type_id;
    }

    public String getTask_type_desc() {
        return task_type_desc;
    }

    public void setTask_type_desc(String task_type_desc) {
        this.task_type_desc = task_type_desc;
    }
}
