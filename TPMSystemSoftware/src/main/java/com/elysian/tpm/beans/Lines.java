package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class Lines implements Serializable {

    private String line_id;
    private String line_desc;
    private int t_status_id;
    private int value_stream_id;
    private int t_flag_id;

    private Date insert_date;
    private String insert_username;
    private Date update_date;
    private String update_username;

    public String getLine_id() {
        return line_id;
    }

    public void setLine_id(String line_id) {
        this.line_id = line_id;
    }

    public String getLine_desc() {
        return line_desc;
    }

    public void setLine_desc(String line_desc) {
        this.line_desc = line_desc;
    }

    public int getT_status_id() {
        return t_status_id;
    }

    public void setT_status_id(int t_status_id) {
        this.t_status_id = t_status_id;
    }

    public int getValue_stream_id() {
        return value_stream_id;
    }

    public void setValue_stream_id(int value_stream_id) {
        this.value_stream_id = value_stream_id;
    }

    public int getT_flag_id() {
        return t_flag_id;
    }

    public void setT_flag_id(int t_flag_id) {
        this.t_flag_id = t_flag_id;
    }

    public Date getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Date insert_date) {
        this.insert_date = insert_date;
    }

    public String getInsert_username() {
        return insert_username;
    }

    public void setInsert_username(String insert_username) {
        this.insert_username = insert_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }
}


