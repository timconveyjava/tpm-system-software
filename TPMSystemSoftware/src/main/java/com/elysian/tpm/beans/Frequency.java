package com.elysian.tpm.beans;

import java.io.Serializable;

public class Frequency implements Serializable {

    private int frequency_id;
    private String frequency_description;

    public Frequency() {
    }

    public Frequency(int frequency_id, String frequency_description) {
        this.frequency_id = frequency_id;
        this.frequency_description = frequency_description;

    }

    public int getFrequency_id() {
        return frequency_id;
    }

    public void setFrequency_id(int frequency_id) {
        this.frequency_id = frequency_id;
    }

    public String getFrequency_description() {
        return frequency_description;
    }

    public void setFrequency_description(String frequency_description) {
        this.frequency_description = frequency_description;
    }
}
