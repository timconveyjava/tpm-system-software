package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class ValueStream implements Serializable {

    private int value_stream_int;
    private String value_stream_name;
    private int area_id;
    private int t_status_id;
    private String short_name;

    private Date insert_date;
    private String insert_username;
    private Date update_date;
    private String update_username;

    public int getValue_stream_int() {
        return value_stream_int;
    }

    public void setValue_stream_int(int value_stream_int) {
        this.value_stream_int = value_stream_int;
    }

    public String getValue_stream_name() {
        return value_stream_name;
    }

    public void setValue_stream_name(String value_stream_name) {
        this.value_stream_name = value_stream_name;
    }

    public int getArea_id() {
        return area_id;
    }

    public void setArea_id(int area_id) {
        this.area_id = area_id;
    }

    public int getT_status_id() {
        return t_status_id;
    }

    public void setT_status_id(int t_status_id) {
        this.t_status_id = t_status_id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public Date getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Date insert_date) {
        this.insert_date = insert_date;
    }

    public String getInsert_username() {
        return insert_username;
    }

    public void setInsert_username(String insert_username) {
        this.insert_username = insert_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }
}
