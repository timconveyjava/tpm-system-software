package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class TaskExecutionSessions implements Serializable {

    private int task_execution_session_id;
    private int task_version_id;

    private Date start_date;
    private Date end_date;

    private String remarks_text;

    private Date skipped_date;
    private String skipped_reason;

    private int current_week;

    public int getTask_execution_session_id() {
        return task_execution_session_id;
    }

    public void setTask_execution_session_id(int task_execution_session_id) {
        this.task_execution_session_id = task_execution_session_id;
    }

    public int getTask_version_id() {
        return task_version_id;
    }

    public void setTask_version_id(int task_version_id) {
        this.task_version_id = task_version_id;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getRemarks_text() {
        return remarks_text;
    }

    public void setRemarks_text(String remarks_text) {
        this.remarks_text = remarks_text;
    }

    public Date getSkipped_date() {
        return skipped_date;
    }

    public void setSkipped_date(Date skipped_date) {
        this.skipped_date = skipped_date;
    }

    public String getSkipped_reason() {
        return skipped_reason;
    }

    public void setSkipped_reason(String skipped_reason) {
        this.skipped_reason = skipped_reason;
    }

    public int getCurrent_week() {
        return current_week;
    }

    public void setCurrent_week(int current_week) {
        this.current_week = current_week;
    }
}
