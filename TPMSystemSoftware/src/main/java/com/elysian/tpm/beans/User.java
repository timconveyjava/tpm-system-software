package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    private int user_id;
    private String username;
    private String first_name;
    private String last_name;
    private String email;

    private int user_status;
    private String status;

    private Date insert_date;
    private Date update_date;

    private int user_role;
    private String role;

    private int updated_by;
    private int role_int;

    public User() {
    }

    public User(int user_id, String username, String first_name, String last_name, String email) {
        this.user_id = user_id;
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
    }

    public User(int user_id, String username, int role) {
        this.user_id = user_id;
        this.username = username;
        this.role_int = role;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getT_status() {
        return user_status;
    }

    public void setT_status(int t_status) {
        this.user_status = t_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Date insert_date) {
        this.insert_date = insert_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public int getT_role() {
        return user_role;
    }

    public void setT_role(int t_role) {
        this.user_role = t_role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(int updated_by) {
        this.updated_by = updated_by;
    }
    
    public int getRole_int() {
        return role_int;
    }

    public void setRole_int(int role_int) {
        this.role_int = role_int;
    }
}
