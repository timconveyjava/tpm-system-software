package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class Area implements Serializable {

    private int area_id;
    private String area_name;
    private int factory_id;
    private int t_status_id;
    private String short_name;

    private Date insert_date;
    private String insert_username;
    private Date update_date;
    private String update_username;

    public int getArea_id() {
        return area_id;
    }

    public void setArea_id(int area_id) {
        this.area_id = area_id;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public int getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(int factory_id) {
        this.factory_id = factory_id;
    }

    public int getT_status_id() {
        return t_status_id;
    }

    public void setT_status_id(int t_status_id) {
        this.t_status_id = t_status_id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public Date getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Date insert_date) {
        this.insert_date = insert_date;
    }

    public String getInsert_username() {
        return insert_username;
    }

    public void setInsert_username(String insert_username) {
        this.insert_username = insert_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }
}
