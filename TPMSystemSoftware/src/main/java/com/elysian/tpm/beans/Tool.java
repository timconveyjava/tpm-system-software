package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class Tool implements Serializable {

    private int tool_id;
    private String tool_name;
    private int tool_nr;
    private int t_status_id;
    private String short_name;
    private int machine_id;

    private Date insert_date;
    private String insert_username;
    private Date update_date;
    private String update_username;

    public int getTool_id() {
        return tool_id;
    }

    public void setTool_id(int tool_id) {
        this.tool_id = tool_id;
    }

    public String getTool_name() {
        return tool_name;
    }

    public void setTool_name(String tool_name) {
        this.tool_name = tool_name;
    }

    public int getTool_nr() {
        return tool_nr;
    }

    public void setTool_nr(int tool_nr) {
        this.tool_nr = tool_nr;
    }

    public int getT_status_id() {
        return t_status_id;
    }

    public void setT_status_id(int t_status_id) {
        this.t_status_id = t_status_id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public int getMachine_id() {
        return machine_id;
    }

    public void setMachine_id(int machine_id) {
        this.machine_id = machine_id;
    }

    public Date getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Date insert_date) {
        this.insert_date = insert_date;
    }

    public String getInsert_username() {
        return insert_username;
    }

    public void setInsert_username(String insert_username) {
        this.insert_username = insert_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }
}
