package com.elysian.tpm.beans;

import java.io.Serializable;

public class Factory implements Serializable {

    private int factory_id;
    private String factory_name;

    public Factory() {
    }

    public Factory(int factory_id, String factory_name) {
        this.factory_id = factory_id;
        this.factory_name = factory_name;
    }

    public int getFactory_id() {
        return factory_id;
    }

    public void setFactory_id(int factory_id) {
        this.factory_id = factory_id;
    }

    public String getFactory_name() {
        return factory_name;
    }

    public void setFactory_name(String factory_name) {
        this.factory_name = factory_name;
    }
}
