package com.elysian.tpm.beans;

import java.io.Serializable;

public class TaskVersionPhoto implements Serializable {

    private int task_version_photo_id;
    private int task_version_id;
    private String task_photo_path;

    public int getTask_version_photo_id() {
        return task_version_photo_id;
    }

    public void setTask_version_photo_id(int task_version_photo_id) {
        this.task_version_photo_id = task_version_photo_id;
    }

    public int getTask_version_id() {
        return task_version_id;
    }

    public void setTask_version_id(int task_version_id) {
        this.task_version_id = task_version_id;
    }

    public String getTask_photo_path() {
        return task_photo_path;
    }

    public void setTask_photo_path(String task_photo_path) {
        this.task_photo_path = task_photo_path;
    }
}
