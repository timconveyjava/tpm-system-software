package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class TaskVersion implements Serializable {

    private int task_version_id;
    private int task_id;
    private int current_version;

    private Date creation_date;
    private String creator_username;
    private Date update_date;
    private String update_username;

    private int frequence_id;
    private int execution_time;
    private String remarks_text;
    private String task_details;
    private int task_path_pdf;
    private int task_photo_id;

    private int first_week;
    private Date first_year;
    private Date first_week_date;

    public int getTask_version_id() {
        return task_version_id;
    }

    public void setTask_version_id(int task_version_id) {
        this.task_version_id = task_version_id;
    }

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public int getCurrent_version() {
        return current_version;
    }

    public void setCurrent_version(int current_version) {
        this.current_version = current_version;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreator_username() {
        return creator_username;
    }

    public void setCreator_username(String creator_username) {
        this.creator_username = creator_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }

    public int getFrequence_id() {
        return frequence_id;
    }

    public void setFrequence_id(int frequence_id) {
        this.frequence_id = frequence_id;
    }

    public int getExecution_time() {
        return execution_time;
    }

    public void setExecution_time(int execution_time) {
        this.execution_time = execution_time;
    }

    public String getRemarks_text() {
        return remarks_text;
    }

    public void setRemarks_text(String remarks_text) {
        this.remarks_text = remarks_text;
    }

    public String getTask_details() {
        return task_details;
    }

    public void setTask_details(String task_details) {
        this.task_details = task_details;
    }

    public int getTask_path_pdf() {
        return task_path_pdf;
    }

    public void setTask_path_pdf(int task_path_pdf) {
        this.task_path_pdf = task_path_pdf;
    }

    public int getTask_photo_id() {
        return task_photo_id;
    }

    public void setTask_photo_id(int task_photo_id) {
        this.task_photo_id = task_photo_id;
    }

    public int getFirst_week() {
        return first_week;
    }

    public void setFirst_week(int first_week) {
        this.first_week = first_week;
    }

    public Date getFirst_year() {
        return first_year;
    }

    public void setFirst_year(Date first_year) {
        this.first_year = first_year;
    }

    public Date getFirst_week_date() {
        return first_week_date;
    }

    public void setFirst_week_date(Date first_week_date) {
        this.first_week_date = first_week_date;
    }
}
