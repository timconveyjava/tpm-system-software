package com.elysian.tpm.beans;

import java.io.Serializable;

public class GlobalSettings implements Serializable {

    private int global_settings_id;
    private String global_settings_desc;

    private static int minimum_timer = 1;

    public int getGlobal_settings_id() {
        return global_settings_id;
    }

    public void setGlobal_settings_id(int global_settings_id) {
        this.global_settings_id = global_settings_id;
    }

    public String getGlobal_settings_desc() {
        return global_settings_desc;
    }

    public void setGlobal_settings_desc(String global_settings_desc) {
        this.global_settings_desc = global_settings_desc;
    }

    public static int getMinimum_timer() {
        return minimum_timer;
    }

    public static void setMinimum_timer(int minimum_timer) {
        GlobalSettings.minimum_timer = minimum_timer;
    }
}
