package com.elysian.tpm.beans;

import java.io.Serializable;
import java.util.Date;

public class Machine implements Serializable {

    private int machine_id;
    private String machine_name;
    private int machine_number;
    private int t_status_id;
    private String line_id;
    private String short_name;
    private int t_flag_id;

    private Date insert_date;
    private String insert_username;
    private Date update_date;
    private String update_username;

    public int getMachine_id() {
        return machine_id;
    }

    public void setMachine_id(int machine_id) {
        this.machine_id = machine_id;
    }

    public String getMachine_name() {
        return machine_name;
    }

    public void setMachine_name(String machine_name) {
        this.machine_name = machine_name;
    }

    public int getMachine_number() {
        return machine_number;
    }

    public void setMachine_number(int machine_number) {
        this.machine_number = machine_number;
    }

    public int getT_status_id() {
        return t_status_id;
    }

    public void setT_status_id(int t_status_id) {
        this.t_status_id = t_status_id;
    }

    public String getLine_id() {
        return line_id;
    }

    public void setLine_id(String line_id) {
        this.line_id = line_id;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public int getT_flag_id() {
        return t_flag_id;
    }

    public void setT_flag_id(int t_flag_id) {
        this.t_flag_id = t_flag_id;
    }

    public Date getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Date insert_date) {
        this.insert_date = insert_date;
    }

    public String getInsert_username() {
        return insert_username;
    }

    public void setInsert_username(String insert_username) {
        this.insert_username = insert_username;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getUpdate_username() {
        return update_username;
    }

    public void setUpdate_username(String update_username) {
        this.update_username = update_username;
    }
}
