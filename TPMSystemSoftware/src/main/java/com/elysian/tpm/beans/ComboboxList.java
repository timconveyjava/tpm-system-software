package com.elysian.tpm.beans;

import java.io.Serializable;

public class ComboboxList implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2899466861738914230L;

    private String label;
    private String value;

    public ComboboxList(String value, String label) {
        super();
        this.label = label;
        this.value = value;
    }

    public ComboboxList() {

    }

    @Override
    public String toString() {
        return "ComboboxList [label=" + label + ", value=" + value + "]";
    }

    // getters and setters

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
