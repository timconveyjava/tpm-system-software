package com.elysian.tpm.services;

import com.elysian.tpm.mybatis.config.DataSourceEnvironment;
import com.elysian.tpm.mybatis.config.MyBatisUtil;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskService implements Serializable {

    private Logger LOGGER = LogManager.getLogger(TaskService.class);

    private static final String GET_FREQUENCY = "com.elysian.tpm.mappers.TaskMapper.getFrequencyList";
    private static final String GET_TASK_TYPE = "com.elysian.tpm.mappers.TaskMapper.getTaskTypeList";
    private static final String GET_MACHINE_NAME = "com.elysian.tpm.mappers.TaskMapper.getMachineName";
    private static final String GET_MACHINE_NUMBER = "com.elysian.tpm.mappers.TaskMapper.getMachineNumber";
    //private static final String GET_TIME = "com.elysian.tpm.mappers.TaskMapper.getTaskDuration";
    private static final String GET_TOOL_NAME = "com.elysian.tpm.mappers.TaskMapper.getToolName";
    private static final String GET_TOOL_NUMBER = "com.elysian.tpm.mappers.TaskMapper.getToolNumber";
    private static final String GET_LINE = "com.elysian.tpm.mappers.TaskMapper.getLineList";

    public List<String> getFrequencyList() {
        List<String> frequency = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            frequency = session.selectList(GET_FREQUENCY);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return frequency;
    }

    public List<String> getTaskTypeList() {
        List<String> task_type = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            task_type = session.selectList(GET_TASK_TYPE);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return task_type;
    }

    public List<String> getMachineNameList() {
        List<String> task_type = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            task_type = session.selectList(GET_MACHINE_NAME);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return task_type;
    }

    public List<String> getMachineNumberList() {
        List<String> task_type = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            task_type = session.selectList(GET_MACHINE_NUMBER);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return task_type;
    }

//    public List<Integer> getTaskDuration() {
//        List<Integer> time = new ArrayList<>();
//        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
//            final Map<String, Object> params = new HashMap<String, Object>();
//
//            time = session.selectList(GET_TIME);
//
//        } catch (final PersistenceException e) {
//            LOGGER.error(e.getMessage());
//        }
//
//        return time;
//    }

    public List<String> getToolNameList() {
        List<String> task_type = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            task_type = session.selectList(GET_TOOL_NAME);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return task_type;
    }

    public List<String> getToolNumberList() {
        List<String> task_type = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            task_type = session.selectList(GET_TOOL_NUMBER);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return task_type;
    }

    public List<String> getLineList() {
        List<String> line = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            line = session.selectList(GET_LINE);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return line;
    }
}
