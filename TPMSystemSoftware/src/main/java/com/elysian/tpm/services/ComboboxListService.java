package com.elysian.tpm.services;

import com.elysian.tpm.beans.ComboboxList;
import com.elysian.tpm.mybatis.config.DataSourceEnvironment;
import com.elysian.tpm.mybatis.config.MyBatisUtil;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
public class NewsService extends GenericService<LinesNews, Integer> implements Serializable {
*/

public class ComboboxListService implements Serializable {
    private Logger LOGGER = LogManager.getLogger(ComboboxListService.class);

    private static final String GET_EXISTING_NEWS = "com.elysian.tpm.mappers.ComboboxListMapper.getExistingNews";
    private static final String GET_AVAILABLE_LINES = "com.elysian.tpm.mappers.ComboboxListMapper.getAvailableLines";

/*
    public NewsService(Class<LinesNews> type) {
        super(type);
    }
*/

    public List<ComboboxList> getExistingNews() {

        List<ComboboxList> news = new ArrayList<>();

        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            news = session.selectList(GET_EXISTING_NEWS);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return news;
    }

    public List<ComboboxList> getAvailableLines() {

        List<ComboboxList> lines = new ArrayList<>();

        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            lines = session.selectList(GET_AVAILABLE_LINES);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return lines;
    }

}
