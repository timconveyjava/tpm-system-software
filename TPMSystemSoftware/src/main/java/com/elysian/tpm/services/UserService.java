package com.elysian.tpm.services;

import com.elysian.tpm.beans.User;
import com.elysian.tpm.mybatis.SQLResult;
import com.elysian.tpm.mybatis.config.DataSourceEnvironment;
import com.elysian.tpm.mybatis.config.MyBatisUtil;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
public class UserService extends GenericService<User, Integer> implements Serializable {
*/
public class UserService implements Serializable {

    private Logger LOGGER = LogManager.getLogger(UserService.class);

    private static final String CHECK_USERNAME_AT_LOGIN = "com.elysian.tpm.mappers.LoginMapper.checkUserAtLogin";

    private static final String GET_USER_BY_USERNAME = "com.elysian.tpm.mappers.UserMapper.getUserByUsername";
    private static final String GET_USERS = "com.elysian.tpm.mappers.UserMapper.getUsers";

    private static final String ADD_USER = "com.elysian.tpm.mappers.UserMapper.addUser";
    private static final String EDIT_USER = "com.elysian.tpm.mappers.UserMapper.editUser";
    private static final String EDIT_USER_ROLE = "com.elysian.tpm.mappers.UserMapper.editUserRole";

    private static final String CHANGE_STATUS = "com.elysian.tpm.mappers.UserMapper.changeStatus";
  /*  public UserService(Class type) {
        super(type);
    }*/

    public boolean checkUserAtLogin(String username) {
        int count = 0;
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            count = session.selectOne(CHECK_USERNAME_AT_LOGIN, username.toLowerCase());

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return count > 0;
    }

    public User getUserByUsername(String username) {
        User user = new User();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            user = session.selectOne(GET_USER_BY_USERNAME, username.toLowerCase());

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return user;
    }

    public List<User> getUsersList() {
        List<User> users = new ArrayList<>();
        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {
            final Map<String, Object> params = new HashMap<String, Object>();

            users = session.selectList(GET_USERS);

        } catch (final PersistenceException e) {
            LOGGER.error(e.getMessage());
        }

        return users;
    }


    public SQLResult addUser(User user) {
        final SQLResult results = new SQLResult();

        final Map<String, Object> params = new HashMap<String, Object>();


        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {

            params.put("p_username", user.getUsername());
            params.put("p_firstname", user.getFirst_name());
            params.put("p_lastname", user.getLast_name());
            params.put("p_email", user.getEmail());
            params.put("p_user_role", user.getRole_int());
            params.put("p_updated_by", user.getUpdated_by());

            session.update(ADD_USER, params);

            session.commit();
            results.sqlProcState(true, null);

        } catch (
                final PersistenceException e) {
            results.sqlProcState(false, e);
            LOGGER.error(e.getMessage());
        }

        return results;
    }

    public SQLResult editUser(User user) {
        final SQLResult results = new SQLResult();

        final Map<String, Object> params = new HashMap<String, Object>();


        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {

            params.put("p_user_id", user.getUser_id());
            params.put("p_username", user.getUsername());
            params.put("p_firstname", user.getFirst_name());
            params.put("p_lastname", user.getLast_name());
            params.put("p_email", user.getEmail());
            params.put("p_updated_by", user.getUpdated_by());

            session.update(EDIT_USER, params);

            session.commit();
            results.sqlProcState(true, null);

        } catch (
                final PersistenceException e) {
            results.sqlProcState(false, e);
            LOGGER.error(e.getMessage());
        }

        return results;
    }

    public SQLResult editUserRole(User user) {
        final SQLResult results = new SQLResult();

        final Map<String, Object> params = new HashMap<String, Object>();


        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {

            params.put("p_user_id", user.getUser_id());
            params.put("p_username", user.getUsername());
            params.put("p_role", user.getRole_int());
            params.put("p_updated_by", user.getUpdated_by());

            session.update(EDIT_USER_ROLE, params);

            session.commit();
            results.sqlProcState(true, null);

        } catch (
                final PersistenceException e) {
            results.sqlProcState(false, e);
            LOGGER.error(e.getMessage());
        }

        return results;
    }
    
    public SQLResult updateStatus(int user_id, int t_status, int updated_by) {
        final SQLResult results = new SQLResult();

        final Map<String, Object> params = new HashMap<String, Object>();


        try (SqlSession session = MyBatisUtil.getUpdatedSession(DataSourceEnvironment.POSTGRES_S.toString())) {

            params.put("p_user_id", user_id);
            params.put("p_t_status", t_status);
            params.put("p_updated_by", updated_by);
            session.update(CHANGE_STATUS, params);

            session.commit();
            results.sqlProcState(true, null);

        } catch (
                final PersistenceException e) {
            results.sqlProcState(false, e);
            LOGGER.error(e.getMessage());
        }

        return results;
    }
}
