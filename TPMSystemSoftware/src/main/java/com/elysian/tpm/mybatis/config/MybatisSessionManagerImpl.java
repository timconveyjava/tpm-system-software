/*******************************************************************************
 * Copyright (c) 2018 Elysian-Software.
 * All rights reserved.
 *
 *******************************************************************************/
package com.elysian.tpm.mybatis.config;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MybatisSessionManagerImpl extends MybatisSessionManager {

    private static Logger logger = LogManager.getLogger(MybatisSessionManagerImpl.class.getName());

    @Override
    public SqlSession openSession(SqlSessionFactory sqlSessionFactory) {
        final SqlSession session = sqlSessionFactory.openSession();
       // logger.info("opened session");
        return session;
    }
}
