/*******************************************************************************
 * Copyright (c) 2018 Elysian-Software.
 * All rights reserved.
 *
 *******************************************************************************/
package com.elysian.tpm.mybatis.config;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class MyBatisUtil {
    private static Map<String, Object> blankParamMap;
    private static MybatisSessionManager msm;

    transient private static Logger LOGGER = LogManager.getLogger(MyBatisUtil.class.getName());
    private static SqlSessionFactory factoryPostgres_S;


    public final static String POSTGRES_S = "POSTGRES_S";

    static {
        Reader reader = null;
        try {
            final String resource = "com/elysian/tpm/mybatis/config/mybatis-config.xml";
            reader = Resources.getResourceAsReader(resource);
            msm = (MybatisSessionManager) Class.forName("com.elysian.tpm.mybatis.config.MybatisSessionManagerImpl")
                    .newInstance();

            factoryPostgres_S = new SqlSessionFactoryBuilder().build(reader, POSTGRES_S);

/*

            reader = Resources.getResourceAsReader(resource)
            factoryOracle = new SqlSessionFactoryBuilder().build(reader, ORACLE);

            reader = Resources.getResourceAsReader(resource);
            factoryPostgres_W = new SqlSessionFactoryBuilder().build(reader, POSTGRES_W);

             reader = Resources.getResourceAsReader(resource);
            factoryPostgres_L = new SqlSessionFactoryBuilder().build(reader, POSTGRES_L);

            reader = Resources.getResourceAsReader(resource);
            factoryPostgres_SB = new SqlSessionFactoryBuilder().build(reader, POSTGRES_SB);*/
            //    LOGGER.trace("msm: " + msm);

        } catch (final IOException e) {
            throw new RuntimeException(e.getMessage());
        } catch (final InstantiationException e) {
            e.printStackTrace();
        } catch (final IllegalAccessException e) {
            e.printStackTrace();
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
        }
        // BlankMap Initialization;
        blankParamMap = new HashMap<String, Object>();
        blankParamMap.put("hasFilters", "no");
        blankParamMap.put("sort", "false");
        //    LOGGER.trace("msm: " + msm);
    }

    public static SqlSessionFactory getSqlSessionFactory(String environment) {
  /*      if (environment.toLowerCase().equals("oracle")) {
            return factoryOracle;
        } else if (environment.toLowerCase().equals("postgres_w")) {
            return factoryPostgres_W;
        } else if (environment.toLowerCase().equals("postgres_l")) {
            return factoryPostgres_L;
        } else if (environment.toLowerCase().equals("postgres_sb")) {
            return factoryPostgres_SB;
        }*/
        return factoryPostgres_S;
    }

    public static void setParamMap(Map<String, Object> paramMap) {
        paramMap.put("hasFilters", "no");
        paramMap.put("sort", "false");
    }

    public static Map<String, Object> getBlankParamMap() {
        return blankParamMap;
    }

    public static SqlSession getUpdatedSession(String environment) {
        return msm.openSession(MyBatisUtil.getSqlSessionFactory(environment));
    }

}
