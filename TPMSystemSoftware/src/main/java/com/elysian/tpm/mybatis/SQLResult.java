package com.elysian.tpm.mybatis;

import java.io.Serializable;
import java.util.List;

public class SQLResult implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6742174167708252307L;

	private boolean ok;
	private String resultMessage;
	private List<Object> resultList;
	private Object singleValue;
	private String stackTrace;

	public final static String SQL_PROC_EXEC_CORRECTLY = "sqlProcExCorrect";
	public final static String SQL_PROC_EXEC_FAILED = "sqlProcExFailed";

	public void sqlProcState(boolean executionSucceded, Exception e) {
		if (executionSucceded) {
			setOk(true);
			setResultMessage(SQL_PROC_EXEC_CORRECTLY);
		} else {
			setOk(false);
			setResultMessage(SQL_PROC_EXEC_FAILED);
			String error = e.getCause().toString();
			setStackTrace(error);
			e.printStackTrace();
		}
	}

	public void sqlProcValue(Object value) {
		resultList = null;
		setSingleValue(value);
	}

	// Getters & Setters
	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public List<Object> getResultList() {
		return resultList;
	}

	public void setResultList(List<Object> resultList) {
		this.resultList = resultList;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public Object getSingleValue() {
		return singleValue;
	}

	public void setSingleValue(Object singleValue) {
		this.singleValue = singleValue;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}
}
