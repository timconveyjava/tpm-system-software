/*******************************************************************************
 * Copyright (c) 2018 Elysian-Software.
 * All rights reserved.
 *
 *******************************************************************************/
package com.elysian.tpm.mybatis.config;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public abstract class MybatisSessionManager {

    public abstract SqlSession openSession(SqlSessionFactory sqlSessionFactory);
}
